<?php

// Plugin definition
$plugin = array(
  'title' => t('Three column 25/50/25 stacked - Content first'),
  'category' => t('Content first'),
  'icon' => 'panels_cf_threecol_25_50_25_stacked.png',
  'theme' => 'panels_cf_threecol_25_50_25_stacked',
  'css' => 'panels_cf_threecol_25_50_25_stacked.css',
  'regions' => array(
    'top' => t('Top'),
    'left' => t('Left side'),
    'middle' => t('Middle column'),
    'right' => t('Right side'),
    'bottom' => t('Bottom'),
  ),
);
